from peewee import Model
from .. import database

class Base(Model):
    class Meta:
        database = database
