import datetime
import peewee as pw
from . import Base


class Customer(Base):
    parse_date = pw.DateTimeField(default=datetime.datetime.now)

    gz_id  = pw.CharField(unique=True)
    rnn    = pw.CharField()
    iin    = pw.CharField()
    bin    = pw.CharField()
    name   = pw.CharField()
    region = pw.CharField()


class CustomerContact(Base):
    parse_date = pw.DateTimeField(default=datetime.datetime.now)

    fio    = pw.CharField()
    mail   = pw.CharField()
    phone  = pw.CharField()
    mobile = pw.CharField()

    customer = pw.ForeignKeyField(Customer, related_name='contacts', null=True)


class Offer(Base):
    parse_date = pw.DateTimeField(default=datetime.datetime.now)

    gz_id             = pw.CharField(unique=True)
    gz_customer_id    = pw.CharField()
    gz_lotname        = pw.CharField()
    gz_ktru           = pw.CharField()
    gz_planned_amount = pw.CharField()
    gz_status         = pw.CharField()
    gz_start_date     = pw.CharField()
    gz_end_date       = pw.CharField()
    is_parsed         = pw.BooleanField(default=False)

    customer = pw.ForeignKeyField(Customer, related_name='offer_lots', null=True)


def up():
    Offer.create_table(fail_silently=True)
    Customer.create_table(fail_silently=True)
    CustomerContact.create_table(fail_silently=True)


def down():
    Offer.drop_table()
    Customer.drop_table()
    CustomerContact.drop_table()