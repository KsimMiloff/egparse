import pundle
pundle.activate()

from egparse import app

if __name__ == "__main__":
    app.run()