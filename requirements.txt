flask == 0.10
flask-assets
flask-mako
plim
flask-wtf
lxml
peewee
arnold
requests
grab
captcha_solver == 0.0.3