from peewee import SqliteDatabase, Model
from flask import g
from egparse import app
from config import DATABASE

db = SqliteDatabase(DATABASE)

class Base(Model):
    class Meta:
        database = db

    def to_dict(self):
        return self._data

    def to_json(self):
        from flask import jsonify
        return jsonify(self.to_dict())


    # @classmethod
    # def select(self, *selection):
    #     from peewee import SelectQuery as SQ
    #
    #     class SelectQuery(SQ):
    #
    #         def to_dicts(self):
    #             dicts = []
    #             for dict in self.dicts():
    #                 dicts.append(dict)
    #             return dicts
    #
    #     query = SelectQuery(self, *selection)
    #     if self._meta.order_by:
    #         query = query.order_by(*self._meta.order_by)
    #     return query


@app.before_request
def before_request():
    g.db = db
    g.db.connect()

@app.after_request
def after_request(response):
    if not g.db.is_closed():
        g.db.close()
    return response
