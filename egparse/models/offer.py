import datetime
import peewee as pw
from egparse.models import Base
from egparse.models.customer import Customer

OFFER_STATUSES = {
    "4" : 'Опубликован',
    "5" : 'В ожидании',
    "7" : 'Исполнен',
    "8" : 'Вскрыт',
    "9" : 'Отменен',
    "10": 'Отказ ГЗ'
}

DATA_URL = 'http://portal.goszakup.gov.kz/portal/index.php/ru/publictrade/openlots/'

class Offer(Base):
    data_url = DATA_URL

    parse_date = pw.DateTimeField(default=datetime.datetime.now)

    gz_id             = pw.CharField(unique=True)
    gz_customer_id    = pw.CharField()
    gz_lotname        = pw.CharField()
    gz_ktru           = pw.CharField()
    gz_planned_amount = pw.CharField()
    gz_status         = pw.CharField()
    gz_start_date     = pw.CharField()
    gz_end_date       = pw.CharField()
    is_parsed         = pw.BooleanField(default=False)

    customer = pw.ForeignKeyField(Customer, related_name='offer_lots', null=True)




