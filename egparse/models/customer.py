import datetime
import peewee as pw
from . import Base


class Customer(Base):
    parse_date = pw.DateTimeField(default=datetime.datetime.now)

    gz_id  = pw.CharField(unique=True)
    rnn    = pw.CharField()
    iin    = pw.CharField()
    bin    = pw.CharField()
    name   = pw.CharField()
    region = pw.CharField()


class CustomerContact(Base):
    parse_date = pw.DateTimeField(default=datetime.datetime.now)

    fio    = pw.CharField()
    mail   = pw.CharField()
    phone  = pw.CharField()
    mobile = pw.CharField()

    customer = pw.ForeignKeyField(Customer, related_name='contacts', null=True)
