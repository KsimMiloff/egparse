from flask.ext.wtf import Form
from wtforms import TextField, BooleanField, StringField, SelectField
from wtforms.validators import Required


class OffersFilterForm(Form):

    from .models.offer import OFFER_STATUSES

    statuses = []
    for key, value in OFFER_STATUSES.items():
        statuses.append([key, value])

    lot_name  = StringField('Наименование, № Лота', validators=[Required()])
    lotstatus = SelectField('Статус лота', choices=statuses, validators=[Required()])
