from flask import Blueprint, request, url_for
from flask import jsonify
from flask.ext.mako import render_template

from egparse.utils.spiders.offer import OfferSpider
from egparse.forms import OffersFilterForm
from egparse.models.offer import Offer

offers = Blueprint('offers', __name__,
                    url_prefix='/offers/')


@offers.route('/', methods=['POST', 'GET'])
def index():
    form = OffersFilterForm(request.form)

    if request.method == 'POST' and form.validate():
        send_filters(request.form)

    return render_template('/lots/offers.plim',
                           form=form)


@offers.route('list', defaults={'ext': None})
@offers.route('list<ext>')
def list(ext):

    lots = Offer.select().dicts()

    if ext and ext == '.json':
        return jsonify(lots=lots[:])


def send_filters(data):

    data = data.to_dict()
    data.pop('csrf_token')
    post_data = {}

    for key in data.keys():
        filter_key = "filter[{}]".format(key)
        post_data[filter_key] = data[key]

    bot = OfferSpider(post_data=post_data)
    bot.run()

