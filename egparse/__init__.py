from flask import Flask
from flask.ext.mako import MakoTemplates

app  = Flask(__name__)
mako = MakoTemplates(app)

# избегаем циклической зависимости
from .views.offers import offers
from .utils import assets

app.config.from_object('config')
# app.config.from_pyfile('instance/__init__.py')

app.register_blueprint(offers)
