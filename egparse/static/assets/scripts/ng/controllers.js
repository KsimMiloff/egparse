var egparseApp = angular.module('egparseApp', ['ui.bootstrap']);

egparseApp.controller('OffersListCtrl', ['$scope', '$http', function($scope, $http) {
  $http.get('/offers/list.json').success(function(data) {
    $scope.offers = data['lots'];
    console.log(data['lots']);
  });

   $scope.orderProp = 'site_id';
   $scope.reverse = true;
   $scope.order = function(orderProp) {
     $scope.reverse = ($scope.orderProp === orderProp) ? !$scope.reverse : false;
     $scope.orderProp = orderProp;
   };
}]);

egparseApp.controller('RefreshOffersCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.click = function($scope, $http) {
      alert();
    };
}]);


egparseApp.controller('ModalDemoCtrl', function ($scope, $modal, $log) {

  $scope.items = ['item1', 'item2', 'item3'];

  $scope.open = function (size) {

    var modalInstance = $modal.open({
      animation: false,
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };


});

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

egparseApp.controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});