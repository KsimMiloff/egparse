from flask.ext.assets import Bundle, Environment
from egparse import app


bundles = {

    # 'front_js': Bundle(
    #     'assets/scripts/lib/angular.js',
    #     output='front.js'),
    #
    'ng_js': Bundle(
        'vendor/scripts/angular.js',
        'vendor/scripts/ui-bootstrap.js',
        'assets/scripts/ng/*',
        output='glued/ng.js'),

    'front_css': Bundle(
        # 'assets/styles/lib/reset.css',
        'vendor/styles/bootstrap.css',
        # 'assets/styles/front/style.scss',
        filters='scss',
        output='glued/front.css')

    # 'admin_js': Bundle(
    #     '',
    #     output='gen/admin.js'),
    # 
    # 'admin_css': Bundle(
    #     'styles/lib/reset.css',
    #     'styles/common.css',
    #     'styles/admin.css',
    #     output='gen/admin.css')
}

# 
# coffee = assets.Bundle('**/*.coffee', filters='coffeescript', output="app.js")

env = Environment(app)
env.register(bundles)