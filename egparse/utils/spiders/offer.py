import logging

logger = logging.getLogger('grab')
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.DEBUG)

from grab.spider import Spider, Task
from captcha_solver import CaptchaSolver

from egparse.utils.spiders.captcha_solver import *
from egparse.models import db
from egparse.models.offer import Offer
from egparse.models.customer import Customer, CustomerContact


class OfferSpider(Spider, CaptchaSolverInterface):

    #https://github.com/alexey-grom/shoutcast/blob/master/parser.py

    def __init__(self, post_data, *kargs, **kwargs):
        super(OfferSpider, self).__init__(*kargs, **kwargs)
        self.post_data = post_data

    def prepare(self):
        self.solver = CaptchaSolver('browser', 'grab')

    def task_generator(self):
        grab = self.create_grab_instance()
        grab.setup(post=self.post_data, url=Offer.data_url)

        yield Task('pages', grab=grab)

    def task_pages(self, grab, task):
        url_list  = []


        if captcha_found(grab):
            # meta = {'handler': self.captcha_handler, grab=grab}
            yield Task('download_captcha', grab=grab)

        if grab.doc.select(".//*[@class='paginator']/a").exists():
            paginator = grab.doc.select(".//*[@class='paginator']").one()

            last_a     = paginator.select(".//a")[-1].node()
            url_pieces = last_a.attrib['href'].split('/')

            pages_count = int(url_pieces[-1])
            url_partial = ('/').join(url_pieces[:-1])

            for page in range(pages_count):
                url_list.append("{}/{}".format(url_partial, page + 1))

        url_list = url_list or [Offer.data_url]
        url_list = [Offer.data_url]

        # content = grab.doc.select(".//*[@id='content']")
        # print(content.text())

        cookies = {}
        for cookie in grab.cookies.items():
            cookies[cookie[0]] = cookie[1]

        # print(cookies)

        for url in url_list:
            grab = grab.clone()
            grab.setup(url=url, cookies=cookies)
            yield Task('lots', grab=grab)


    def task_captcha(self, grab, task):
        pass


    def task_lots(self, grab, task):
        data = []

        tbl_selector = ".//table[@class='zebra']"

        content = grab.doc.select(".//*[@id='content']")
        # print(content.text())

        if grab.doc.select(tbl_selector).exists():
            table = grab.doc.select(tbl_selector).one()
            trs   = table.select('.//tr')

            if trs:
                for tr in trs:

                    offer_attrs = {}
                    tds = tr.select('.//td')

                    attrs_to_parse = ['gz_id', 'gz_customer_id', 'gz_lotname', 'gz_ktru', 'gz_planned_amount', 'gz_status', 'gz_start_date', 'gz_end_date']
                    customer_href  = None

                    for index, td in enumerate(tds):

                        value = td.text()

                        attr = attrs_to_parse[index]
                        offer_attrs[attr] = value

                        if attr == 'gz_customer_id':
                            customer_href = td.select('a/@href').text()
                            value = customer_href.split('/')[-1]
                            offer_attrs[attr] = value

                        elif index == 6: # if start_date
                            offer_attrs['gz_start_date'] = td.select( 'br/following-sibling::text()' ).text()
                            offer_attrs['gz_end_date']   = td.select( 'br/preceding-sibling::text()' ).text()

                    if set(attrs_to_parse).issubset(offer_attrs):
                        # print('valid data here:')

                        offer_sample = Offer.select().where(Offer.gz_id == offer_attrs['gz_id'])
                        if offer_sample.exists():
                            offer = offer_sample.first()
                            # print("offer : `{}` is already exists".format(offer_sample.first()))
                        else:
                            offer = Offer(**offer_attrs)
                            offer.save()

                            print('save offer is success!!!')

                        customer_sample = Customer.select().where(Customer.gz_id == offer.gz_customer_id)
                        if not customer_sample.exists() and customer_href:
                            yield Task('customer', url=customer_href)
                            # break

                        if not offer.customer_id:
                            offer.customer = customer_sample.first()
                            offer.save()

                        print('my customer is:')
                        print(offer.customer_id)


                        data.append(offer_attrs)

                    # yield Task('habrapost', url=elem.get('href'))

        print('collected data:')
        print(len(data))


    def task_customer(self, grab, task):
        tbl_selector = ".//div[@class='points']/table"
        tables = grab.doc.select(tbl_selector)

        signs = {
            'rnn': 'РНН',
            'bin': 'БИН',
            'iin': 'ИИН',
            'name': 'Наименование на русском языке',
            'region': 'Регион',
        }

        gz_id = grab.response.url.split('/')[-1]

        customer_data = {'gz_id': gz_id}
        contacts_data = []

        if tables.exists():
            main_table = tables[0]
            contacts_table = None

            for i, table in enumerate(tables):
                cols_count = len(table.select('.//tr')[0].select('.//td | .//th'))
                if cols_count == 4:
                    contacts_table = tables[i]
                    break

            trs = main_table.select('.//tr')

            for tr in trs:
                tds = tr.select('.//td')

                for key in signs.keys():
                    sign = signs[key]

                    if tds[0].text() == sign:
                        customer_data[key] = tds[1].text()

            customer = Customer.create(**customer_data)

            if contacts_table:
                trs = contacts_table.select('.//tr')
                for tr in trs:
                    tds = tr.select('.//td')
                    if len(tds) == 4:
                        contacts_data.append({
                            'fio': tds[0].text(),
                            'mail': tds[1].text(),
                            'phone': tds[2].text(),
                            'mobile': tds[3].text(),
                            'customer_id': customer.id
                        })

                if contacts_data:
                    for contact_data in contacts_data:
                        CustomerContact.create(**contact_data)