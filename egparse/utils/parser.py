# from lxml import html
# import requests
#
# import locale
# locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
#
# class Parser:
#
#     @classmethod
#     def offers(self, url=''):
#
#         content    = self.__get_page(self, url)
#         data_table = content.find(".//table[@class='zebra']")
#         paginator  = content.find(".//*[@class='paginator']")
#
#         url_list = []
#         if paginator:
#             last_a      = paginator.findall(".//a")[-1]
#             url_pieces  = last_a.attrib['href'].split('/')
#             pages_count = int(url_pieces[-1])
#             url_partial = ('/').join(url_pieces[:-1])
#
#             for page in range(pages_count):
#                 url_list.append("{}/{}".format(url_partial, page + 1))
#
#         data = self.__read_table(self, data_table)
#
#         # if url_list:
#         #     for url in url_list:
#         #         data_table = self.__get_page_table(self, url)
#         #         data += self.__read_table(self, data_table)
#
#         print(len(data))
#
#         return data
#
#
#     def __get_page(self, url):
#         print(url)
#
#         filter = {"filter[lot_name]": "калоприемник"}
#
#         data = requests.post(url, params=filter)
#         root = html.fromstring(data.text)
#         print(data)
#         print(data.text)
#
#
#         content = root.find(".//*[@id='content']")
#
#         return content
#
#
#     def __get_page_table(self, url):
#         content = self.__get_page(self, url)
#         return content.find(".//table[@class='zebra']")
#
#
#     def __read_table(self, data_table):
#         data = []
#         trs  = data_table.findall(".//tr")
#
#         for tr in trs:
#             row = {}
#             tds = tr.findall('.//td')
#
#             for index, td in enumerate(tds):
#
#                 if index == 6:
#                     value = td.xpath('text()')
#                 else:
#                     value = td.xpath('string()')
#
#                 if index == 0:
#                     row['site_id'] = value
#                 elif index == 1:
#                     href  = td.find('a').get('href')
#                     value = href.split('/')[-1]
#                     row['customer_id'] = value
#                 elif index == 2:
#                     row['lotname'] = value
#                 elif index == 3:
#                     row['ktru'] = value
#                 elif index == 4:
#                     value = value.replace(' ', '').replace(',', '.')
#                     row['planned_amount'] = locale.atof(value)
#                 elif index == 5:
#                     row['status'] = value
#                 elif index == 6:
#                     row['start_date'] = value[0]
#                     row['end_date']   = value[1]
#
#             if len(row) == 8:
#                 data.append(row)
#                 # print(row)
#
#         return data
#
#
#     def competition(self):
#         None
#
#     def auction(self):
#         None
