import re
from plim import preprocessor_factory
from plim.lexer import parse_tag_tree
from plim.syntax import BaseSyntax


def parse_handlebars(indent_level, current_line, ___, source, syntax):
    """
    :param indent_level:
    :param current_line:
    :param ___:
    :param source:
    :param syntax: an instance of one of :class:`plim.syntax.BaseSyntax` children.
    :type syntax: :class:`plim.syntax.BaseSyntax`
    :return:
    """
    processed_tag, tail_indent, tail_line, source = parse_tag_tree(indent_level, current_line, ___, source, syntax)
    assert processed_tag.startswith("<handlebars") and processed_tag.endswith("</handlebars>")
    # We don't want to use str.replace() here, therefore
    # len("<handlebars") == len("handlebars>") == 11
    processed_tag = '<script {content}script>'.format(
        content=processed_tag[11:-11]
    )
    return processed_tag, tail_indent, tail_line, source

CUSTOM_PARSERS = [
    (BaseSyntax.PARSE_HANDLEBARS_RE, parse_handlebars)
]


preprocessor = preprocessor_factory(custom_parsers=CUSTOM_PARSERS, syntax='mako')