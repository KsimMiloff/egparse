from config.plim import preprocessor


DEBUG = True # Turns on debugging features in Flask

MAKO_IMPORTS = ['from egparse.utils.assets import env', 'from egparse.forms import OffersFilterForm']
MAKO_PREPROCESSOR = preprocessor

CSRF_ENABLED = True
SECRET_KEY = 'aple4ko'

DATABASE = 'egparse.db'